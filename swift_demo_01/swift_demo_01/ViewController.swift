//
//  ViewController.swift
//  swift_demo_01
//
//  Created by Alice on 2021/10/10.
//

import UIKit
import SnapKit
import Kingfisher

class ViewController: UIViewController {
    
    //商品封面
    private lazy var imgCover: UIImageView = {
        let img = UIImageView()
        //圆角
        img.layer.cornerRadius = 5
        img.layer.masksToBounds = true
        //边线宽度
        img.layer.borderWidth = 1
        img.layer.borderColor = UIColor.white.cgColor
        //居中裁剪模式
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    //商品名字
    private lazy var labGoodsName: UILabel = {
        let lab = UILabel()
        //最多显示2行
        lab.numberOfLines = 2
        lab.font = UIFont.boldSystemFont(ofSize: 14)
        lab.textColor = ColorUtils.parser("#151617")
        return lab
    }()
    
    //原价
    private lazy var labOriginalPrice: UILabel = {
        let lab = UILabel()
        lab.textColor = ColorUtils.parser("#9D9E9F")
        lab.font = lab.font.withSize(10)
        //停靠左边
        lab.textAlignment = NSTextAlignment.left
        return lab
    }()
    //最后价格
    private lazy var labPrice: UILabel = {
        let lab = UILabel()
        lab.font = lab.font.withSize(12)
        lab.textColor = ColorUtils.parser("#F35410")
        return lab
    }()
    //店铺名字
    private lazy var labStoreName: UILabel = {
        let lab = UILabel()
        lab.font = lab.font.withSize(10)
        lab.textColor = ColorUtils.parser("#9B9C9D")
        return lab
    }()
     


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("hello ios")
        //let lab = UILabel(frame:
        //                    CGRect(x: 100,
        //                           y: 100,
        //                           width: 300,
        //                           height: 100
        //                    )
        //)
        
        let lab = UILabel()
        lab.text = "ios 的第一个程序"
        lab.textColor = UIColor.green
        self.view.addSubview(lab)
        // 约束控制位置在中间
        lab.snp.makeConstraints{
            // 约束这个lab的中心等同于父视图的中心
            $0.center.equalToSuperview()
        }
        //在约束之前,需要先添加到主视图中.
        self.view.addSubview(imgCover)
        self.view.addSubview(labGoodsName)
        self.view.addSubview(labOriginalPrice)
        self.view.addSubview(labPrice)
        self.view.addSubview(labStoreName)
        
        //布局左边的封面图片
        imgCover.snp.makeConstraints { (make) in
            //另一种闭包写法
            //ios有安全区的概念,就是刘海区域之下,底部白条之上属于安全区.
            //默认布局是在屏幕的左上角,这个依赖安全区来显示.
            //封面的上面依赖安全区的顶部
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            //左边是根布局的左边,相距左边12个点
            make.left.equalToSuperview().offset(12)
            //图片大小120
            make.size.equalTo(120)
        }
        
        // 图片url
        let url = URL(string: "https://images.sunofbeaches.com/content/2021_10_31/904413721286148096.png")
        //引入图片加载框架Kingfisher
        imgCover.kf.setImage(with: url)
        //名字
        labGoodsName.snp.makeConstraints { (make) in
            make.left.equalTo(imgCover.snp.right).offset(10)
            make.top.equalTo(imgCover.snp.top).offset(5)
            make.right.equalToSuperview().offset(-10)
        }
        labGoodsName.text = "三年之约,云岚之巅,云韵"
        
        //店铺名字
        labStoreName.snp.makeConstraints { (make) in
            make.left.equalTo(imgCover.snp.right).offset(10)
            make.bottom.equalTo(imgCover.snp.bottom)
        }
        labStoreName.text = "断点专卖店"
        
        //优惠之后的价格
        labPrice.snp.makeConstraints { (make) in
            make.left.equalTo(imgCover.snp.right).offset(10)
            make.bottom.equalTo(labStoreName.snp.top).offset(-3)
        }
        labPrice.text = "¥29.99"
        
        //优惠之前的价格
        labOriginalPrice.snp.makeConstraints { (make) in
            make.left.equalTo(labPrice.snp.right).offset(4)
            make.bottom.equalTo(labPrice.snp.bottom)
        }
        labOriginalPrice.text = "39.99"
    }
}

