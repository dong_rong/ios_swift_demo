//
//  HttpDemoController.swift
//  swift_demo_01
//
//  Created by Alice on 2021/11/29.
//

import Alamofire
import UIKit

class HttpDemoController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // 请求数据,我的博客文章列表5.4.4,5.0.2
        print("开始请求数据")
        let url = "http://www.debuglive.cn/portal/article/list/1/5"
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success(let value):
                print("请求成功", value)
            case .failure(let error):
                print("请求失败", error)
            }
        }
    }
}
